import time
import json
import app.repository.blockumentRepository as blockument_repository
import app.repository.userRepository as user_repository

from flask_restful import Resource, reqparse
from app.lib import security as security_lib
from app import logger, blockchain

import app.lib.validation as validation_lib

class CheckBlockchainController(Resource):
    def __init__(self):
        parser = reqparse.RequestParser()
        parser.add_argument('blockument_id', help='Used to find or update documents', required=True)
        parser.add_argument('username', help='Used to find documents', required=False)
        parser.add_argument('password', help='Used to find documents', required=False)

        self.args = parser.parse_args(strict=True)

    def post(self):
        checked_user = None
        try:
            token = security_lib.check_jwt_auth_token()
            if token is False:
                checked_user = user_repository.get_user(self.args.username)
                if checked_user is not None:
                    user_id = checked_user.get_id()
                    if security_lib.check_password(checked_user.get_password(), self.args.password) is False:
                        checked_user = None
            else:
                checked_user = json.loads(token.get('user'))
            


            if checked_user is not None:
                blockument = blockument_repository.find_blockument_by_id(self.args.blockument_id)
                if blockument is not None:
                    access = validation_lib.check_access_to_document(user_id, self.args.blockument_id, blockument.get_author())
                    if access[0] is False:
                        return {'message': 'You don\'t have access to this document.'}, 403

                else:
                    return {'message':'Document not found.'}, 400

                history = blockchain.check_block(self.args.blockument_id)
                if history is not False:
                    nb_reads = 0
                    nb_updates = 0
                    nb_downloads = 0
                    readers = []
                    alerts = []

                    reads_list = []
                    updates_list = []
                    downloads_list = []

                    history = json.loads(history)
                    for event in history:
                        #print(event)
                        if event.get('data').get('action') == 'update':
                            nb_updates += 1
                            
                            user = user_repository.get_user_by_id(event.get('data').get('author'))
                            if user is not None:
                                author_name = user.get_lastname()
                                if user.get_lastname() not in readers:
                                    readers.append(user.get_lastname())
                            else:
                                author_name=None
                                alerts.append('unkown user accessed the blockument')

                            update = {
                                'author': author_name,
                                'date': event.get('data').get('date')
                            }
                            
                            updates_list.append(update)
                        elif event.get('data').get('action') == 'read':
                            user = user_repository.get_user_by_id(event.get('data').get('author'))
                            if user is not None:
                                author_name = user.get_lastname()
                                if user.get_lastname() not in readers:
                                    readers.append(user.get_lastname())
                            else:
                                author_name = None
                                alerts.append('unkown user accessed the blockument')
                            read = {
                                'author': author_name,
                                'date': event.get('data').get('date')
                            }
                            print(read)
                            reads_list.append(read)
                            nb_reads +=1

                        elif event.get('data').get('action') == 'download':
                            user = user_repository.get_user_by_id(event.get('data').get('author'))
                            if user is not None:
                                author_name = user.get_lastname()
                                if user.get_lastname() not in readers:
                                    readers.append( user.get_lastname())
                            else:
                                author_name = None
                                alerts.append('unkown user accessed the blockument')
                            download = {
                                'author': author_name,
                                'date': event.get('data').get('date')
                            }
                            downloads_list.append(download)
                            nb_downloads +=1
                    
                    response = {
                        'nb_reads' : nb_reads,
                        'nb_updates' : nb_updates,
                        'nb_downloads': nb_downloads,
                        'readers' : readers,
                        'reads_list': reads_list,
                        'updates_list': updates_list,
                        'downloads_list': downloads_list,
                        'alerts': alerts
                    }
                    return {'message' : 'Blockument found', 'history': response}, 200
                else:
                    return {'message': 'Blockument not found'}, 500   
            else:
                return {'message':'Something Wrong happened'}, 500                
        except Exception as e:
            print(e)
            return {'message':'Something Wrong happened'}, 500