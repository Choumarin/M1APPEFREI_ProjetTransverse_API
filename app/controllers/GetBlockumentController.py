import time
import json
import app.repository.blockumentRepository as blockument_repository
import app.lib.validation as validation_lib

from flask_restful import Resource, reqparse
from app.repository import userRepository as user_repository
from app.lib import security as security_lib
from app import logger, blockchain

class GetBlockumentController(Resource):
    def __init__(self):
        parser = reqparse.RequestParser()
        parser.add_argument('blockument_id', help='Used to find documents', required=False)
        parser.add_argument('username', help='Used to find documents', required=False)
        parser.add_argument('password', help='Used to find documents', required=False)
        self.args = parser.parse_args(strict=True)
    
    def post(self):
        try:
            token = security_lib.check_jwt_auth_token()
            if token is False:
                try:
                    checked_user = user_repository.get_user(self.args.username)
                    if checked_user is not None:
                        user_id = checked_user.get_id()
                        if  security_lib.check_password(checked_user.get_password(), self.args.password):
                            if self.args.blockument_id :
                                data = {
                                    "date": time.time(),
                                    "id": str(self.args.blockument_id),
                                    "action": "read",
                                    "author": user_id        
                                }

                                blockchain.send_block(data)

                                try:
                                    blockument = blockument_repository.find_blockument_by_id(self.args.blockument_id)
                                    if blockument is not None:
                                        access = validation_lib.check_access_to_document(user_id, self.args.blockument_id, blockument.get_author())
                                        print(access)
                                        if access[0]:
                                            return{'blockument': blockument.get_dict(), 'access': access[1]}, 200
                                        else:
                                            return{'message': 'You don\'t have access to this document.'}, 403
                                    else:
                                        return {'message' : 'Blockument not found'}, 404
                                except Exception as e:
                                    print(e)
                            else:
                                try:
                                    blockuments_list = blockument_repository.find_blockuments_for_user(user_id)
                                    return {'blockuments_list':  blockuments_list}, 200
                                except Exception as e:
                                    print(e)
                                    return {'message':'Something Wrong happened'}, 500
                except Exception as e:
                    print(e)
                    return {'message':'Something Wrong happened'}, 500
            else:
                try:
                    user = json.loads(token.get('user'))
                    if self.args.blockument_id :
                        data = {
                            "date": time.time(),
                            "id": str(self.args.blockument_id),
                            "action": "read",
                            "author": user['id']        
                        }

                        blockchain.send_block(data)
                        try:
                            blockument = blockument_repository.find_blockument_by_id(self.args.blockument_id)
                            access = validation_lib.check_access_to_document(user['id'], self.args.blockument_id, blockument.get_id())
                            if access[0]:
                                if blockument is not None:
                                    return{'blockument': blockument.get_dict(), 'access': access[1]}, 200
                                else:
                                    return{'message': 'You don\'t have access to this document.'}, 403
                        except Exception as e:
                            print(e)
                    else:
                        blockuments_list = blockument_repository.find_blockuments_for_user(user['id'])
                        return {'blockuments_list':  blockuments_list}, 200
                except Exception as e:
                    print(e)
                    return {'message':'Something Wrong happened'}, 500
        except Exception as e:
                print(e)
                return {'message':'Something Wrong happened'}, 500
                
        return {'message': 'invalid token'}, 400