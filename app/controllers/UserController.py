import re
from flask_restful import Resource, reqparse
from app import logger
from app.entity import user as user_entity
from app.repository import userRepository as user_repository
from app.lib import security as security_lib
from app.lib import validation as validation_lib


class UserController(Resource):
    def __init__(self):
        parser = reqparse.RequestParser()
        parser.add_argument('username', help='This field cannot be blank', required=True)
        parser.add_argument('password', help='This field cannot be blank', required=True)
        parser.add_argument('confirmed_password', help='This field cannot be blank', required=True)
        parser.add_argument('name', help='This field cannot be blank', required=True)
        parser.add_argument('lastname', help='This field cannot be blank', required=True)
        parser.add_argument('email', help='This field cannot be blank', required=True)
        parser.add_argument('role', help='This field cannot be blank', required=True)
        self.args = parser.parse_args(strict=True)

    def post(self):
        logger.info('request on /signup, method : POST')
        pattern = re.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,32}$')
        if self.args.password == self.args.confirmed_password and pattern.match(self.args.password):
            try:
                valid = validation_lib.check_user_signup(self.args.username, self.args.email)
                if valid[0] is True :
                    password = security_lib.encrypt_password(self.args.password)
                    username = self.args.username
                    name = self.args.name
                    lastname = self.args.lastname
                    mail = self.args.email
                    if self.args.role == 'patient':
                        validated = True
                    else:
                        validated = False
                    user = user_entity.User(name=name, lastname=lastname, username=username, mail=mail, password=password, role = self.args.role, validated=validated)

                    user_repository.create_user(user)
                    logger.info('user added')
                    return {'message': 'user registered'}, 200
                else:
                    return {'message' : valid[1]}, 500
            except Exception as e:
                logger.error(str(e))
                return {'message': 'error: something wrong happened'}, 500
        else:
            return {'message':'password not confirmed'}, 500
