import time
import json
import app.repository.blockumentRepository as blockument_repository
import app.repository.userRepository as user_repository
import app.lib.validation as validation_lib

from flask_restful import Resource, reqparse
from app.lib import security as security_lib
from app import logger


class SetSharesController(Resource):
    def __init__(self):
        parser = reqparse.RequestParser()
        parser.add_argument('blockument_id', help='Used to find or update documents', required=True)
        parser.add_argument('username_shared', help='Used to find or update documents', required=True)
        #parser.add_argument('right', help='Used to find or update documents', required=True)
        parser.add_argument('username', help='Used to find documents', required=False)
        parser.add_argument('password', help='Used to find documents', required=False)
        self.args = parser.parse_args(strict=True)

    def post(self):
        token = security_lib.check_jwt_auth_token()
        checked_user = None
        if token is False:
            checked_user = user_repository.get_user(self.args.username)
            user_id = str(checked_user.get_id())
            
            if checked_user is not None:
                if  security_lib.check_password(checked_user.get_password(), self.args.password) is False:
                    checked_user = None
        else:
            checked_user = json.loads(token.get('user'))
            user_id = checked_user['id']

        blockument = blockument_repository.find_blockument_by_id(self.args.blockument_id)

        if blockument is not None:
            
            access = validation_lib.check_access_to_document(user_id, self.args.blockument_id, blockument.get_author())
            if access[0] is False:
                return {'message': 'You don\'t have access to this blockument'}, 403 
        else:
            return {'message':'Blockument not found'}, 400
            
        if checked_user is not None:
            found_user_share = user_repository.get_user(self.args.username_shared)
            if found_user_share is not None:
                user_share_id = found_user_share.get_id()
                user_share_access = validation_lib.check_access_to_document(str(user_share_id), self.args.blockument_id, blockument.get_author())
                if user_share_access[0] is not False:
                    return {'message': 'Blockument already shared with this user'}, 500
                blockument_repository.share_blockument(self.args.blockument_id, str(user_share_id), str(user_id))
                return {'message': 'Blockument shared'}, 200
            else:
                return {'message': 'The provided user was not found'}, 400
        else: 
            return {'message': 'token is not valid'}

        return {'message':'Something Wrong happened'}, 500
        
