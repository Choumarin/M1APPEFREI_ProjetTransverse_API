import json

from flask import session
from flask_restful import Resource, reqparse
from app.repository import userRepository as user_repository
from app.lib import security as security_lib
from app import logger


class AuthenticationController(Resource):
    def __init__(self):
        parser = reqparse.RequestParser()
        parser.add_argument('username', help='This field cannot be blank', required=True)
        parser.add_argument('password', help='This field cannot be blank', required=True)
        self.args = parser.parse_args(strict=True)

    def post(self):
        try:
            logger.info('request on /login, method : POST')
            checked_user = user_repository.get_user(self.args.username)
            if checked_user is not None :
                crendentials_validity = security_lib.check_password(checked_user.get_password(), self.args.password)
                if crendentials_validity:
                    if checked_user.get_validated() is not True:
                        return {'message': 'user account not validated'}, 403

                    # creation du token jwt
                    checked_user.set_password(None)

                    user = json.dumps(checked_user.getDict())
                    token = security_lib.generate_jwt_auth_token(user)
                    session['token'] = token
                    session.modified = True
                    logger.info('authentication succeed')
                    return {'message': 'authenticated', 'token': token.decode('utf8').replace("'", '"')}
        except Exception as e:
            logger.error(str(e))

        return {'message': 'error : wrong username or password'}, 403
