import time
import json
import app.repository.blockumentRepository as blockument_repository
import app.repository.userRepository as user_repository

from flask_restful import Resource, reqparse
from app.lib import security as security_lib
from app import logger


class BlockumentController(Resource):
    def __init__(self):
        parser = reqparse.RequestParser()
        parser.add_argument('blockument_id', help='Used to find or update documents', required=False)
        parser.add_argument('type', help='Used to find or update documents', required=True)
        parser.add_argument('content', help='Used to find or update documents', required=True)
        parser.add_argument('username', help='Used to find documents', required=False)
        parser.add_argument('password', help='Used to find documents', required=False)

        self.args = parser.parse_args(strict=True)

    def post(self):
        token = security_lib.check_jwt_auth_token()
        checked_user = None
        if token is False:
            checked_user = user_repository.get_user(self.args.username)
            user_id = checked_user.get_id()
            if checked_user is not None:
                if  security_lib.check_password(checked_user.get_password(), self.args.password) is False:
                    checked_user = None
        else:
            checked_user = json.loads(token.get('user'))
            user_id = checked_user['id']

        if self.args.type is not None:
            if self.args.blockument_id is not None:
                logger.info('request on /blockument, method : POST, action: update')
                last_update_date = time.time()
                blockument = blockument_repository.find_blockument_by_id(self.args.blockument_id)
                blockument.set_last_update_author(user_id)
                blockument.set_last_update_date(last_update_date)
                blockument.set_content(self.args.content) 
                blockument_repository.update_blockument(blockument)
                return {'message': 'Blockument updated', 'blockument': blockument.get_dict()}, 200
            else:
                logger.info('request on /blockument, method : POST, action: create')
                creation_date = time.time()
                created_blockument = blockument_repository.create_blockument(user_id, creation_date, self.args.type, self.args.content)
                return {'message': 'Blockument created', 'blockument': created_blockument.get_dict()}, 200