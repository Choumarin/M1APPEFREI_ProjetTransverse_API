import time
import json
import app.repository.blockumentRepository as blockument_repository
import app.repository.userRepository as user_repository

from flask_restful import Resource, reqparse
from app.lib import security as security_lib
from app import logger, blockchain


class NotifyDownloadController(Resource):
    def __init__(self):
        parser = reqparse.RequestParser()
        parser.add_argument('blockument_id', help='Used to find documents', required=True)
        parser.add_argument('username', help='Used to find documents', required=False)
        parser.add_argument('password', help='Used to find documents', required=False)
        self.args = parser.parse_args(strict=True)

    def post(self):
        checked_user = None
        try:
            token = security_lib.check_jwt_auth_token()
            if token is False:
                checked_user = user_repository.get_user(self.args.username)
                if checked_user is not None:
                    user_id = checked_user.get_id()
                    if security_lib.check_password(checked_user.get_password(), self.args.password) is False:
                        checked_user = None
            else:
                checked_user = json.loads(token.get('user'))
                user_id = checked_user['id']
                
            if checked_user is not None:
                data = {
                    "date": time.time(),
                    "id": str(self.args.blockument_id),
                    "action": "download",
                    "author": user_id  
                }
                if blockchain.send_block(data):
                    return {'message': 'Data sent to blockchain'}, 200
        except Exception as e:
            print(e)
        return {'message':'Something Wrong happened'}, 500