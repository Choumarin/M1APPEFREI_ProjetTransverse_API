from app.entity.user import User
from app import mongo_db
from bson.objectid import ObjectId


def get_user(username):
    query = {
        "username": username
    }
    user = mongo_db.user.find_one(query)
    if user is not None:
        return User(username=user['username'], password =user['password'], name = user['name'], lastname = user['lastname'],mail= user['mail'], role=user['role'], validated=user['validated'] ,id = str(user['_id']))
    else:
        return None


def get_user_by_id(user_id):

    query = {
        "_id": ObjectId(user_id)
    }
    user = mongo_db.user.find_one(query)
    if user is not None:
        return User(username=user['username'], password =user['password'], name = user['name'], lastname = user['lastname'],mail= user['mail'], role=user['role'])
    else:
        return None


def create_user(user):
    query = {
        "username": user.get_username(),
        "password": user.get_password(),
        "name": user.get_name(),
        "lastname": user.get_lastname(),
        "mail": user.get_mail(),
        "role": user.get_role(),
        "validated": user.get_validated()
    }
    user_id = mongo_db.user.insert_one(query).inserted_id

    user = get_user_by_id(user_id)
    return user


def update_user(user):
    query = {
        "username": user.get_username(),
        "password": user.get_password(),
        "name": user.get_name(),
        "lastname": user.get_lastname(),
        "mail": user.get_mail(),
        "role": user.get_role()
    }

    mongo_db.user.update_one({"_id": user.get_id()}, {'$set': query})
    user = get_user_by_id(ObjectId(id))
    return user
