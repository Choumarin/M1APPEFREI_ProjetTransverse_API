import time

from app.entity.blockument import Blockument
from app import mongo_db
from bson.objectid import ObjectId
from app import blockchain

def find_blockument_by_id(blockument_id):
    blockument = mongo_db.blockument.find_one({"_id": ObjectId(blockument_id)})
    if blockument is not None:
        result =  Blockument(blockument['author'], blockument['creation_date'], blockument['last_update_author'],
                          blockument['last_update_date'], blockument['type'], id=str(blockument_id))
        if 'content' in blockument:
            result.set_content(blockument['content'])
        return result
    return None


def find_blockuments_for_user(author):
    blockuments_user = mongo_db.blockument.find({"author": author})
    blockument_list = []
    for blockument in blockuments_user:
        blockument_tmp = Blockument(blockument['author'], blockument['creation_date'], blockument['last_update_author'], blockument['last_update_date'], blockument['type'], id = str(blockument['_id']))
        if 'content' in blockument:
            blockument_tmp.set_content(blockument['content'])
        blockument_list.append({'blockument':blockument_tmp.get_dict(), 'rigths': 'RW'})

    shares_list = mongo_db.shares.find({'user_id':author})
    for share in shares_list:
        blockument = find_blockument_by_id(share['document_id'])
        blockument_list.append({'blockument':blockument.get_dict(), 'rights' : share['rights']})
    return blockument_list


def create_blockument(author, creation_date, blockument_type, content):
    query = {
        "author": author,
        "creation_date": creation_date,
        "last_update_author": author,
        "last_update_date" : creation_date,
        "type": blockument_type,
        "content":content
    }

    blockument_id = mongo_db.blockument.insert_one(query).inserted_id

    data = {
            "date": creation_date,
            "id": str(blockument_id),
            "action": "create",
            "author": author        
    }
    
    blockchain.send_block(data)
    
    return find_blockument_by_id(str(blockument_id))


def update_blockument(blockument):
    query = {
        'author': blockument.get_author(),
        'creation_date': blockument.get_creation_date(),
        'last_update_author': blockument.get_last_update_author(),
        'last_update_date': blockument.get_last_update_date(),
        'type': blockument.get_type(),
        "content":blockument.get_content()
    }
     
    data = {
        "date": blockument.get_last_update_date(),
        "id": blockument.get_id(),
        "action": "update",
        "author": blockument.get_last_update_author()
    }

    mongo_db.blockument.update_one({"_id": blockument.get_id()}, {'$set': query})
    blockchain.send_block(data)
    return find_blockument_by_id(ObjectId(id))

def share_blockument(blockument_id, user_share_id, action_author):
    query = {
        'user_id':user_share_id,
        'document_id': blockument_id,
        'rights' : 'R'
    }

    share_id = mongo_db.shares.insert_one(query).inserted_id

    data = {
            "date": time.time(),
            "id": str(blockument_id),
            "action": "share",
            "author": str(action_author),
    }
    
    blockchain.send_block(data)
