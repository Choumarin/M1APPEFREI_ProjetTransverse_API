from app import app, api, api_bp
from flask_cors import CORS
from app.controllers import UserController
from app.controllers import AuthenticationController
from app.controllers import BlockumentController
from app.controllers import GetBlockumentController
from app.controllers import CheckBlockchainController
from app.controllers import NotifyDownloadController
from app.controllers import setSharesController

CORS(app, resources={r"*": {"origins": "*"}})

# Routes for user login and registration
api.add_resource(UserController.UserController, '/signup', endpoint='signup')
api.add_resource(AuthenticationController.AuthenticationController, '/login', endpoint='login')

# Routes used to manipulate blockuments
api.add_resource(BlockumentController.BlockumentController, '/blockument', endpoint='blockument')
api.add_resource(GetBlockumentController.GetBlockumentController, '/get_blockument', endpoint='get_blockument')
api.add_resource(CheckBlockchainController.CheckBlockchainController, '/check_blockchain', endpoint='check_blockchain')
api.add_resource(NotifyDownloadController.NotifyDownloadController, '/notify_download', endpoint='notify_download')
api.add_resource(setSharesController.SetSharesController, '/set_shared', endpoint='set_shared')

app.register_blueprint(api_bp)
    