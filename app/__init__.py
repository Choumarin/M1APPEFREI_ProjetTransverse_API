import os
import logging
from logging.handlers import RotatingFileHandler

from flask import Blueprint
from flask_restful import Api
from flask import Flask
from flask_session import Session
from pymongo import MongoClient
from .lib import config as config_module
from .entity.blockchain import Blockchain

handler = RotatingFileHandler('logs/app.logs', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)

logger = logging.getLogger('api')
logger.setLevel(logging.ERROR)
logger.addHandler(handler)

# Lancement de l'application
app = Flask(__name__)
app.logger.addHandler(handler)
api_bp = Blueprint('api', __name__)

# Check Configuration section for more details
SESSION_TYPE = 'redis'
app.config.from_object(__name__)
Session(app)

api = Api(api_bp)
dir_path = os.path.dirname(os.path.realpath(__file__))
CONFIG = config_module.config(dir_path + '/config.json')

db_config = CONFIG.get_mongo_config()

connection = MongoClient(db_config['uri'])
secret = 'some_secret'
mongo_db = connection['api']

blockchain = Blockchain(CONFIG.get_blockchain_config())
from app import router
