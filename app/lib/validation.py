from app import mongo_db

def check_user_signup(username, email):

    query = {
        "username": username
    }
    user = mongo_db.user.find_one(query)

    if user is not None:
        return [False, 'error : username already used']

    query = {
        "mail": email
    }
    user = mongo_db.user.find_one(query)

    if user is not None:
        return [False, 'error : email already used']
    
    return [True, '']

def check_access_to_document(user_id, document_id, document_author_id):
    print(user_id)
    print(document_author_id)

    if user_id == document_author_id:
        return [True, 'RW']
    else:
        access = mongo_db.shares.find_one({'$and':[{'user_id' : user_id}, {'document_id':document_id}]})
        if access is not None:
            print(access)
            return [True, access.get('rigths')]
    return [False, None]
