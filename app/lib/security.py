import jwt
import datetime
import json

from flask import session
from passlib.hash import bcrypt
from app import secret


def check_password(hashed_password, submited_password):
    return bcrypt.verify(submited_password, hashed_password)


def encrypt_password(password):
    return bcrypt.hash(password)


def generate_jwt_auth_token(user):
    # le token est valide pendant 2 heures
    token = jwt.encode(
        {
            'user': user,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=7200)
        }, '',
        algorithm='HS256'
    )
    return token


def check_jwt_auth_token():
    try:
        if 'token' in session :
            token = jwt.decode(session['token'], '', algorithms=['HS256'])
            return  token
    except jwt.ExpiredSignatureError:
        return False
    except Exception as e :
        print(e)

    return False
