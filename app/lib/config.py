import json


class config:
    def __init__(self, config_file_location):
        with open(config_file_location) as config_data:
            config = json.load(config_data)
            config_data.close()

            self.__mongo = config['mongo']
            self.__blockchain_url=config['blockchain_url']

    def get_mongo_config(self):
        return self.__mongo

    def get_blockchain_config(self):
        return self.__blockchain_url