import requests
import json

class Blockchain():
    def __init__(self, blockchain_url):
        print(blockchain_url)
        self.__blockchain_url = blockchain_url
    
    def send_block(self, body):
        r = requests.post('{0}/mineblock'.format(self.__blockchain_url), json = body)
        if r.status_code == 200:
            return True
        
        return False

    def check_block(self, block_id):
        r = requests.get('{0}/blocks/{1}'.format(self.__blockchain_url, block_id))
        print('{0}/block/{1}'.format(self.__blockchain_url, block_id))
        if r.status_code == 200:
            return r.text
        return False
    