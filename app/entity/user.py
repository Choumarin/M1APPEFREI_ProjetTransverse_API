class User:

    def __init__(self, username, password, name, lastname, mail, role, validated=False, id=None):
        self.__id = str(id)
        self.__username = username
        self.__password = password
        self.__name = name
        self.__lastname = lastname
        self.__mail = mail
        self.__role = role
        self.__validated = validated

    def get_id(self):
        return self.__id

    def get_username(self):
        return self.__username

    def set_username(self, username):
        self.__username = username

    def get_password(self):
        return self.__password

    def set_password(self, password):
        self.__password = password

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = name

    def get_lastname(self):
        return self.__lastname

    def set_lastname(self, lastname):
        self.__lastname = lastname

    def get_mail(self):
        return self.__mail

    def set_mail(self, mail):
        self.__mail = mail

    def get_role(self):
        return self.__role

    def set_role(self, role):
        self.__role = role
    
    def get_validated(self):
        return self.__validated

    def set_validated(self, validated):
        self.__validated = validated

    def getDict(self):
        return {
            'id': self.__id,
            'username': self.__username,
            'name': self.__name,
            'lastname': self.__lastname,
            'email': self.__mail,
            'role': self.__role,
            'validated' : self.__validated
        }
