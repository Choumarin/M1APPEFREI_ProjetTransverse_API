import ast

from app.repository import userRepository as user_repository

class Blockument:
    def __init__(self,
                 author,
                 creation_date,
                 last_update_author,
                 last_update_date,
                 type,
                 id=None,
                 content=None):
        self.__id = id
        self.__author = author
        self.__creation_date = creation_date
        self.__last_update_author = last_update_author
        self.__last_update_date = last_update_date
        self.__type = type
        self.__content = content

    # Geters
    def get_id(self):
        return self.__id

    def get_author(self):
        return self.__author

    def get_creation_date(self):
        return self.__creation_date

    def get_last_update_author(self):
        return self.__last_update_author

    def get_last_update_date(self):
        return self.__last_update_date

    def get_type(self):
        return self.__type

    def get_content(self):
        return self.__content

    # Seters
    def set_author(self, author):
        self.__author = author

    def set_creation_date(self, creation_date):
        self.__creation_date = creation_date

    def set_last_update_author(self, last_update_author):
        self.__last_update_author = last_update_author

    def set_last_update_date(self, last_update_date):
        self.__last_update_date = last_update_date

    def set_type(self, type):
        self.__type = type

    def set_content(self, content):
        self.__content = content

    def get_dict(self):
        user = user_repository.get_user_by_id(self.__author)
        author = self.__author

        if user is not None:
            author = {"lastname": user.get_lastname(), "firstname": user.get_name(), "role": user.get_role()}
        
        last_update_user = user_repository.get_user_by_id(self.__last_update_author)
        last_update_author= self.__last_update_author

        try:
            content_json = ast.literal_eval(self.__content.replace("'", "\""))
        except Exception as e:
            content_json=self.__content
            print(e)

        if last_update_user is not None :
            last_update_author = {"lastname": last_update_user.get_lastname(), "firstname": last_update_user.get_name(), "role": last_update_user.get_role()}
        return {
            "author": author,
            "last_update_author": last_update_author,
            "id": str(self.__id),
            "creation_date": self.__creation_date,
            "last_update_date": self.__last_update_date,
            "content": content_json,
            "type": self.__type
        }
        