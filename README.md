# API FLASK
Le projet est basé sur ce [tutoriel](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world-legacy "tutoriel flask")

# Prérequis:
- Python = 3.6
- virtualenv

## Installation du projet
Instalation de virtualenv
`$ pip install virtualenv`

création du virtualenv
`$ virtualenv -p /usr/bin/python3.6 choumarin`

Lancement du virtualenv (linux/mac)
`$ source choumarin/bin/activate`

Lancement du virtualenv (windows)
`> choumarin\Scripts\activate`

Installation des dépendances
`$ pip install -r requirements.txt`

Lancement de l'application :
`$ python run_server.py` 

Pour sortir du virtualenv:
`$ deactivate`

En cas de problème avec les dépendance, ouvrir une issue avec le message d'erreur affiché lors du lancement du serveur. Les dépendances manquantes seront ajoutés au requirements.txt et il faudra relancer la procédure d'installation des dépendances une fois celle-ci résolue et mergée.