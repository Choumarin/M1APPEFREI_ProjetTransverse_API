import unittest
from app.repository import userRepository
from app.entity import user as user_entity
from bson.objectid import ObjectId

class test_get_user(unittest.TestCase):

    def test(self):
        username = 'test'
        user = userRepository.get_user(username)
        # verif des infos
        self.assertNotEqual(user, None)
        self.assertEqual(user.get_username(), username)


class test_create_user(unittest.TestCase):
    def test(self):
        user_test = user_entity.User(name='paul', lastname='testeur', username='test2', mail='test2@test.com', password='test', role='test')
        user = userRepository.create_user(user_test)
        # verif des infos
        self.assertNotEqual(user, None)
        self.assertEqual(user.get_username(), 'test2')
        self.assertEqual(user.get_password(), 'password')
        self.assertEqual(user.get_name(), 'paul')
        self.assertEqual(user.get_lastname(), 'testeur')
        self.assertEqual(user.get_mail(), 'test2@test.com')
        self.assertEqual(user.get_role(), 'test')

        pass


class test_update_user(unittest.TestCase):
    def test(self):
        user_doc_id = ObjectId(u'5abcddf9dcc5c15e9b2f8041')

        new_email = 'new_test@test.coms'
        new_role = "new_test"
        user = userRepository.update_user(user_doc_id, 'test', 'test', 'test', new_email, new_role)

        #  verif des infos
        self.assertEqual(user.get_username(), 'test')
        self.assertEqual(user.get_password(), 'test')
        self.assertEqual(user.get_name(), 'test')
        self.assertEqual(user.get_lastname(), 'test')
        self.assertEqual(user.get_mail(), new_email)
        self.assertEqual(user.get_role(), new_role)

        new_email = 'test@test.coms'
        new_role = "test"
        # on annule la modif
        user = userRepository.update_user(user_doc_id, 'test', 'test', 'test', new_email, new_role)

class test_get_non_existing_user(unittest.TestCase):
    def test(self):
        username = 'pastest'
        user = userRepository.get_user(username)
        self.assertEqual(user, None)