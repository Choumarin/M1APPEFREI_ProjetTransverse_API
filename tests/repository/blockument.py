import unittest

from datetime import datetime
from app.repository import blockumentRepository


class test_get_blockument_list(unittest.TestCase):
    def test(self):
        user_id = '5abcddf9dcc5c15e9b2f8041'
        blockument_list = blockumentRepository.find_blockuments_for_user(user_id)
        self.assertNotEqual(len(blockument_list), 0)
        self.assertEqual(len(blockument_list), 2)


class test_get_single_blockument_by_id(unittest.TestCase):
    def test(self):
        blockument_id = '5abced17dcc5c15e9b2f8042'
        user_id = '5abcddf9dcc5c15e9b2f8041'
        blockument = blockumentRepository.find_blockument_by_id(blockument_id, user_id)
        self.assertNotEqual(blockument, None)
        self.assertEqual(blockument.get_author(), user_id)
        self.assertEqual(blockument.get_creation_date(), datetime(2018, 3, 1, 0, 0))
        self.assertEqual(blockument.get_last_update_date(), datetime(2018, 3, 1, 0, 0))
        self.assertEqual(blockument.get_last_update_author(), user_id)
        self.assertEqual(blockument.get_type(), 'test')


class test_create_new_blockument(unittest.TestCase):
    def test(self):
        blockument_creation_date = datetime(2018, 3, 1, 0, 0)
        blockument_author = '5abcddf9dcc5c15e9b2f8041'
        blockument_type = ('5abcddf9dcc5c15e9b2f8041')
        blockument = blockumentRepository.create_blockument(blockument_creation_date, blockument_author,
                                                            blockument_type)
        self.assertNotEqual(blockument, None)
        self.assertEqual(blockument.get_author(), blockument_author)
        self.assertEqual(blockument.get_creation_date(), blockument_creation_date)
        self.assertEqual(blockument.get_last_update_date(), blockument_creation_date)
        self.assertEqual(blockument.get_last_update_author(), blockument_author)
        self.assertEqual(blockument.get_type(), blockument_type)


class test_delete_blockument(unittest.TestCase):
    def test(self):
        blockument_id = ''
        user_id = '5abcddf9dcc5c15e9b2f8041'

        blockumentRepository.delete_blockument(blockument_id)
        blockument = blockumentRepository.find_blockument_by_id(blockument_id, user_id)
        self.assertEqual(blockument, None)

class test_get_non_existing_blockument(unittest.TestCase):
    def test(self):
        blockument_id = 'random'
        user_id = '5abcddf9dcc5c15e9b2f8041'
        blockument = blockumentRepository.find_blockument_by_id(blockument_id, user_id)
        self.assertEqual(blockument, None)