#!/usr/bin/python
from app import app, connection, logger


def main():
    try:
        app.run(host='0.0.0.0', port=5000, debug=True)
        logger.info('started app')
    except Exception as e:
        logger.error(str(e))
    finally:
        connection.close()


if __name__ == '__main__':
    main()
